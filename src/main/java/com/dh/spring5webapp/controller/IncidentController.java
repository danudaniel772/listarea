package com.dh.spring5webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dh.spring5webapp.services.IncidentService;

@Controller
public class IncidentController {
    private IncidentService incidentService;

    public IncidentController(IncidentService incidentService) {
        this.incidentService = incidentService;
    }

    @RequestMapping("/incidents")
    public String getIncidents(Model model) {
        model.addAttribute("incidents", incidentService.getIncidents());
        return "incidents";
    }
}