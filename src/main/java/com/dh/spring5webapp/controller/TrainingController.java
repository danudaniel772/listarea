package com.dh.spring5webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dh.spring5webapp.services.TrainingService;

@Controller
public class TrainingController {
    private TrainingService trainingService;

    public TrainingController(TrainingService trainingService) {
        this.trainingService = trainingService;
    }

    @RequestMapping("/trainings")
    public String getTrainings(Model model) {
        model.addAttribute("trainings", trainingService.getTrainings());
        return "trainings";
    }
}