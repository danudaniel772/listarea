package com.dh.spring5webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dh.spring5webapp.services.IncidenRegistryService;

@Controller
public class IncidenRegistryController {
    private IncidenRegistryService incidenRegistryService;

    public IncidenRegistryController(IncidenRegistryService incidenRegistryService) {
        this.incidenRegistryService = incidenRegistryService;
    }

    @RequestMapping("/incidenRegistries")
    public String getIncidenRegistries(Model model) {
        model.addAttribute("incidenRegistries", incidenRegistryService.getIncidenRegistries());
        return "incidenRegistries";
    }
}