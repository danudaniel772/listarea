/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.services;

import java.util.Set;
import com.dh.spring5webapp.model.Training;



public interface TrainingService {

    Set<Training> getTrainings();
}
